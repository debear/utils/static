<?php
    /*
    * Config
    */
    $_DOMAIN = [
        'central' => $_SERVER['SERVER_NAME'],
        'port' => $_SERVER['SERVER_PORT'] == 443 ? '' : ':' . $_SERVER['SERVER_PORT'],
    ];

    $_DIRS = [
        'base' => __DIR__ . '/../_debear/resources',
        'images' => 'images',
        'fonts' => 'fonts',
    ];

    $_MIME = [
        'css' => 'text/css',
        'js' => 'application/javascript',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'otf' => 'font/otf',
        'eot' => 'application/vnd.ms-fontobject',
        'svg' => 'image/svg+xml',
        'ttf' => 'font/ttf',
        'woff' => 'font/woff',
        'woff2' => 'font/woff2',
    ];
