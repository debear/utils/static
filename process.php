<?php
    /*
     * Config
     */
    require __DIR__ . '/config.inc';

    /*
     * Validation
     */
    // Right arguments passed?
    if (!isset($_GET['site']) || !isset($_GET['dir']) || !(isset($_GET['file']) xor isset($_GET['image']) xor isset($_GET['font'])) || !isset($_GET['type'])) {
        invalid_args();
    }

    // List of banned file types
    if (!isset($_MIME[$_GET['type']])) {
        invalid_args();
    }

    // Does the site exist?
    $_DIRS['site'] = $_DIRS['base'] . '/' . $_GET['dir'] . '/' . $_GET['site'];
    if (!file_exists($_DIRS['site'])) {
        invalid_args();
    }

    // File exist?
    if (isset($_GET['file'])) {
        $_FILE = $_DIRS['site'] . '/' . $_GET['file'] . '.' . $_GET['type'];
    } elseif (isset($_GET['image'])) {
        $_FILE = $_DIRS['site'] . '/' . $_GET['image'] . '.' . $_GET['type'];
        // This could plausibly fail, so fallback to the skeleton images
        if (!file_exists($_FILE)) {
            $_FILE = str_replace('/' . $_GET['dir'] . '/' . $_GET['site'], '/' . $_GET['dir'] . '/skel', $_FILE);
        }
    } elseif (isset($_GET['font'])) {
        $_FILE = $_DIRS['site'] . '/' . $_GET['font'] . '.' . $_GET['type'];
    }
    if (!file_exists($_FILE)) {
        invalid_args();
    }

    $is_css = ($_GET['type'] == 'css');

    // Browser caching?
    session_cache_limiter(false);
    header('Cache-Control: private, max-age=43200'); // 43200 = 12 hours
    $headers = function_exists('apache_request_headers') ? apache_request_headers() : [];
    $file_mtime = filemtime($_FILE);
    $file_mtime_head = header_date($file_mtime);
    if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) >= $file_mtime)) {
        header('Last-Modified: ' . $file_mtime_head, true, 304);
        exit;
    }
    // Image isn't cached, so add time to headers
    header('Last-Modified: ' . $file_mtime_head, true, 200);

    // Get the filesize
    if (!$is_css) {
        $filesize = filesize($_FILE);
    } else {
        $contents = str_replace('url(\'/', 'url(\'//' . $_DOMAIN['central'] . $_DOMAIN['port'] . '/' . $_GET['site'] . '/', file_get_contents($_FILE));
        $filesize = strlen($contents);
    }

    // Send the file
    header('Content-Type: ' . $_MIME[$_GET['type']]);
    header("Content-Length: $filesize");
    header('Expires: ' . header_date(strtotime('+1 week')));
    header('Access-Control-Allow-Origin: *');//https://' . str_replace('static', '*', $_DOMAIN['central']) . $_DOMAIN['port']);
    if (!$is_css) {
        readfile($_FILE);
    } else {
        print $contents;
    }

    /*
     * Misc methods
     */
    function invalid_args() {
        http_response_code(404);
        exit;
    }
    function header_date($ts) {
        return gmdate('D, d M Y H:i:s', $ts) . ' GMT';
    }
?>
